EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "AC/DC voltage  conversion"
Date "2020-02-21"
Rev "2.1.1"
Comp "ALEEA"
Comment1 "LONGUET Gilles"
Comment2 "AGPLv3"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L shield-rescue:R-RESCUE-Windlogger-shield-rescue R16
U 1 1 561274D4
P 6875 4500
F 0 "R16" V 6955 4500 40  0000 C CNN
F 1 "470k" V 6875 4525 40  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6805 4500 30  0001 C CNN
F 3 "~" H 6875 4500 30  0000 C CNN
F 4 "0.125W" V 6875 4500 60  0001 C CNN "Puissance"
F 5 "1%" V 6875 4500 60  0001 C CNN "Tolerance"
	1    6875 4500
	0    1    1    0   
$EndComp
$Comp
L shield-rescue:C-RESCUE-Windlogger-shield-rescue C10
U 1 1 561274E3
P 7800 4800
F 0 "C10" H 7800 4900 40  0000 L CNN
F 1 "10uF" H 7806 4715 40  0000 L CNN
F 2 "windlog:CP_Radial_D5.0mm_P2.50mm" H 7838 4650 30  0001 C CNN
F 3 "~" H 7800 4800 60  0000 C CNN
F 4 "16V" H 7800 4800 60  0001 C CNN "Tension"
	1    7800 4800
	1    0    0    -1  
$EndComp
Text HLabel 10525 4050 2    60   Output ~ 0
Uac
Text HLabel 7000 3350 0    60   Input ~ 0
UAC_hi
Wire Wire Line
	9725 4050 10175 4050
Wire Wire Line
	9325 3500 9325 3750
Wire Wire Line
	9325 4350 9325 4600
Wire Wire Line
	8775 4150 9125 4150
Wire Wire Line
	8775 4150 8775 4925
Wire Wire Line
	8775 4925 10175 4925
Wire Wire Line
	10175 4925 10175 4050
Connection ~ 10175 4050
Text Notes 6800 5550 0    79   ~ 0
The AC wattmeter come from OpenEnergyMonitor's works
$Comp
L shield-rescue:R-RESCUE-Windlogger-shield-rescue R17
U 1 1 561E1890
P 7475 3675
F 0 "R17" V 7555 3675 40  0000 C CNN
F 1 "150k" V 7482 3676 40  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7405 3675 30  0001 C CNN
F 3 "~" H 7475 3675 30  0000 C CNN
F 4 "0.125W" V 7475 3675 60  0001 C CNN "Puissance"
F 5 "1%" V 7475 3675 60  0001 C CNN "Tolerance"
	1    7475 3675
	1    0    0    -1  
$EndComp
Wire Wire Line
	8425 3500 8425 3350
Connection ~ 8425 3950
Wire Wire Line
	8425 4500 8425 4700
$Comp
L shield-rescue:+5V-power1 #PWR047
U 1 1 573BFF1D
P 6425 4200
F 0 "#PWR047" H 6425 4050 50  0001 C CNN
F 1 "+5V" H 6425 4340 50  0000 C CNN
F 2 "" H 6425 4200 50  0000 C CNN
F 3 "" H 6425 4200 50  0000 C CNN
	1    6425 4200
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:+5V-power1 #PWR051
U 1 1 573BFF68
P 8425 3350
F 0 "#PWR051" H 8425 3200 50  0001 C CNN
F 1 "+5V" H 8425 3490 50  0000 C CNN
F 2 "" H 8425 3350 50  0000 C CNN
F 3 "" H 8425 3350 50  0000 C CNN
	1    8425 3350
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:+5V-power1 #PWR055
U 1 1 573BFFB3
P 9325 3500
F 0 "#PWR055" H 9325 3350 50  0001 C CNN
F 1 "+5V" H 9325 3640 50  0000 C CNN
F 2 "" H 9325 3500 50  0000 C CNN
F 3 "" H 9325 3500 50  0000 C CNN
	1    9325 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8425 3800 8425 3950
Wire Wire Line
	1750 3025 1825 3025
Text HLabel 1750 3025 0    60   Input ~ 0
Ubat
$Comp
L shield-rescue:R-RESCUE-Windlogger-shield-rescue R15
U 1 1 57E763E5
P 1825 4325
F 0 "R15" V 1905 4325 40  0000 C CNN
F 1 "56k" V 1832 4326 40  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1755 4325 30  0001 C CNN
F 3 "~" H 1825 4325 30  0000 C CNN
F 4 "0.125W" V 1825 4325 60  0001 C CNN "Puissance"
F 5 "1%" V 1825 4325 60  0001 C CNN "Tolerance"
	1    1825 4325
	1    0    0    -1  
$EndComp
Wire Wire Line
	1825 4575 1825 4800
Wire Wire Line
	3550 4075 4000 4075
Wire Wire Line
	2550 4175 2950 4175
Wire Wire Line
	2550 4175 2550 4850
Wire Wire Line
	2550 4850 4000 4850
Wire Wire Line
	4000 4850 4000 4075
Connection ~ 4000 4075
Wire Wire Line
	3150 3475 3150 3775
Wire Wire Line
	3150 4375 3150 4625
Text HLabel 4300 4075 2    60   Output ~ 0
UDC
Wire Wire Line
	1825 3625 1825 3975
Wire Wire Line
	1600 3975 1825 3975
Connection ~ 1825 3975
$Comp
L shield-rescue:R-RESCUE-Windlogger-shield-rescue R14
U 1 1 57E76412
P 1825 3375
F 0 "R14" V 1905 3375 40  0000 C CNN
F 1 "300k" V 1832 3376 40  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1755 3375 30  0001 C CNN
F 3 "~" H 1825 3375 30  0000 C CNN
F 4 "0.125W" V 1825 3375 60  0001 C CNN "Puissance"
F 5 "1%" V 1825 3375 60  0001 C CNN "Tolerance"
	1    1825 3375
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3325 2150 3525
Wire Wire Line
	2150 3825 2150 3975
Connection ~ 2150 3975
Wire Wire Line
	2150 4425 2150 4625
$Comp
L shield-rescue:+5V-power1 #PWR043
U 1 1 57E76433
P 3150 3475
F 0 "#PWR043" H 3150 3325 50  0001 C CNN
F 1 "+5V" H 3150 3615 50  0000 C CNN
F 2 "" H 3150 3475 50  0000 C CNN
F 3 "" H 3150 3475 50  0000 C CNN
	1    3150 3475
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:+5V-power1 #PWR041
U 1 1 57E76439
P 2150 3325
F 0 "#PWR041" H 2150 3175 50  0001 C CNN
F 1 "+5V" H 2150 3465 50  0000 C CNN
F 2 "" H 2150 3325 50  0000 C CNN
F 3 "" H 2150 3325 50  0000 C CNN
	1    2150 3325
	1    0    0    -1  
$EndComp
Text HLabel 7000 3625 0    60   Input ~ 0
UAC_lo
Wire Wire Line
	1825 3025 1825 3125
$Comp
L shield-rescue:GND-power1 #PWR040
U 1 1 5804797B
P 1825 4975
F 0 "#PWR040" H 1825 4725 50  0001 C CNN
F 1 "GND" H 1825 4825 50  0000 C CNN
F 2 "" H 1825 4975 50  0000 C CNN
F 3 "" H 1825 4975 50  0000 C CNN
	1    1825 4975
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR042
U 1 1 580479B9
P 2150 4625
F 0 "#PWR042" H 2150 4375 50  0001 C CNN
F 1 "GND" H 2150 4475 50  0000 C CNN
F 2 "" H 2150 4625 50  0000 C CNN
F 3 "" H 2150 4625 50  0000 C CNN
	1    2150 4625
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR044
U 1 1 580479F7
P 3150 4625
F 0 "#PWR044" H 3150 4375 50  0001 C CNN
F 1 "GND" H 3150 4475 50  0000 C CNN
F 2 "" H 3150 4625 50  0000 C CNN
F 3 "" H 3150 4625 50  0000 C CNN
	1    3150 4625
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR049
U 1 1 58047A51
P 7650 5125
F 0 "#PWR049" H 7650 4875 50  0001 C CNN
F 1 "GND" H 7650 4975 50  0000 C CNN
F 2 "" H 7650 5125 50  0000 C CNN
F 3 "" H 7650 5125 50  0000 C CNN
	1    7650 5125
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR052
U 1 1 58047A8F
P 8425 4700
F 0 "#PWR052" H 8425 4450 50  0001 C CNN
F 1 "GND" H 8425 4550 50  0000 C CNN
F 2 "" H 8425 4700 50  0000 C CNN
F 3 "" H 8425 4700 50  0000 C CNN
	1    8425 4700
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR056
U 1 1 58047ACD
P 9325 4600
F 0 "#PWR056" H 9325 4350 50  0001 C CNN
F 1 "GND" H 9325 4450 50  0000 C CNN
F 2 "" H 9325 4600 50  0000 C CNN
F 3 "" H 9325 4600 50  0000 C CNN
	1    9325 4600
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:D-shield-rescue D7
U 1 1 58047B4E
P 2150 3675
F 0 "D7" H 2150 3775 50  0000 C CNN
F 1 "1N4148" H 2150 3575 50  0000 C CNN
F 2 "windlog:D_DO-35_SOD27_P7.62mm_Horizontal" H 2150 3675 50  0001 C CNN
F 3 "" H 2150 3675 50  0000 C CNN
	1    2150 3675
	0    1    1    0   
$EndComp
$Comp
L shield-rescue:D-shield-rescue D8
U 1 1 58047BD4
P 2150 4275
F 0 "D8" H 2150 4375 50  0000 C CNN
F 1 "1N4148" H 2150 4175 50  0000 C CNN
F 2 "windlog:D_DO-35_SOD27_P7.62mm_Horizontal" H 2150 4275 50  0001 C CNN
F 3 "" H 2150 4275 50  0000 C CNN
	1    2150 4275
	0    1    1    0   
$EndComp
$Comp
L shield-rescue:D-shield-rescue D9
U 1 1 58047F56
P 8425 3650
F 0 "D9" H 8425 3750 50  0000 C CNN
F 1 "1N4148" H 8425 3550 50  0000 C CNN
F 2 "windlog:D_DO-35_SOD27_P7.62mm_Horizontal" H 8425 3650 50  0001 C CNN
F 3 "" H 8425 3650 50  0000 C CNN
	1    8425 3650
	0    1    1    0   
$EndComp
$Comp
L shield-rescue:D-shield-rescue D10
U 1 1 58047F9C
P 8425 4350
F 0 "D10" H 8425 4450 50  0000 C CNN
F 1 "1N4148" H 8425 4250 50  0000 C CNN
F 2 "windlog:D_DO-35_SOD27_P7.62mm_Horizontal" H 8425 4350 50  0001 C CNN
F 3 "" H 8425 4350 50  0000 C CNN
	1    8425 4350
	0    1    1    0   
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR048
U 1 1 58540AD7
P 7100 3675
F 0 "#PWR048" H 7100 3425 50  0001 C CNN
F 1 "GND" H 7100 3525 50  0000 C CNN
F 2 "" H 7100 3675 50  0000 C CNN
F 3 "" H 7100 3675 50  0000 C CNN
	1    7100 3675
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3675 7100 3625
Wire Wire Line
	7100 3625 7000 3625
$Comp
L shield-rescue:CP-shield-rescue C9
U 1 1 58540BFD
P 7200 3350
F 0 "C9" H 7225 3450 50  0000 L CNN
F 1 "1uF" H 7225 3250 50  0000 L CNN
F 2 "windlog:CP_Radial_D5.0mm_P2.50mm" H 7238 3200 50  0001 C CNN
F 3 "" H 7200 3350 50  0000 C CNN
	1    7200 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 3350 7050 3350
$Comp
L shield-rescue:R-RESCUE-Windlogger-shield-rescue R18
U 1 1 561274B6
P 7475 4225
F 0 "R18" V 7555 4225 40  0000 C CNN
F 1 "20k" V 7482 4226 40  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7405 4225 30  0001 C CNN
F 3 "~" H 7475 4225 30  0000 C CNN
F 4 "0.125W" V 7475 4225 60  0001 C CNN "Puissance"
F 5 "1%" V 7475 4225 60  0001 C CNN "Tolerance"
	1    7475 4225
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 3350 7475 3350
Wire Wire Line
	7475 3350 7475 3425
Wire Wire Line
	7475 3925 7475 3950
Wire Wire Line
	7800 5125 7800 5000
Wire Wire Line
	7475 4500 7475 4475
Connection ~ 7475 4500
$Comp
L shield-rescue:R-RESCUE-Windlogger-shield-rescue R19
U 1 1 561274C5
P 7475 4850
F 0 "R19" V 7555 4850 40  0000 C CNN
F 1 "470k" V 7482 4851 40  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7405 4850 30  0001 C CNN
F 3 "~" H 7475 4850 30  0000 C CNN
F 4 "0.125W" V 7475 4850 60  0001 C CNN "Puissance"
F 5 "1%" V 7475 4850 60  0001 C CNN "Tolerance"
	1    7475 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4500 7800 4600
Wire Wire Line
	7475 4600 7475 4500
Connection ~ 7475 3950
Wire Notes Line
	5475 6000 5475 1650
Text Notes 2150 2775 0    60   ~ 0
Batteries monitoring
$Comp
L shield-rescue:C-shield-rescue C7
U 1 1 58542605
P 1600 4325
F 0 "C7" H 1350 4425 50  0000 L CNN
F 1 "100nF" H 1350 4225 50  0000 L CNN
F 2 "windlog:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 1638 4175 50  0001 C CNN
F 3 "" H 1600 4325 50  0000 C CNN
	1    1600 4325
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 4175 1600 3975
Wire Wire Line
	1600 4475 1600 4800
Wire Wire Line
	1600 4800 1825 4800
Connection ~ 1825 4800
Text Notes 725  4125 0    60   ~ 0
Low pass filter
$Comp
L shield-rescue:MCP6002-shield-rescue U2
U 2 1 585910A4
P 9425 4050
F 0 "U2" H 9425 4200 50  0000 L CNN
F 1 "MCP6242" H 9425 3900 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 9325 4100 50  0001 C CNN
F 3 "" H 9425 4200 50  0000 C CNN
	2    9425 4050
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:MCP6004-shield-rescue U1
U 4 1 58593D31
P 3250 4075
F 0 "U1" H 3300 4275 50  0000 C CNN
F 1 "MCP6244" H 3400 3875 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 3200 4175 50  0001 C CNN
F 3 "" H 3300 4275 50  0000 C CNN
	4    3250 4075
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR054
U 1 1 585943E0
P 8825 2025
F 0 "#PWR054" H 8825 1775 50  0001 C CNN
F 1 "GND" H 8825 1875 50  0000 C CNN
F 2 "" H 8825 2025 50  0000 C CNN
F 3 "" H 8825 2025 50  0000 C CNN
	1    8825 2025
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:+5V-power1 #PWR053
U 1 1 585943E6
P 8825 1125
F 0 "#PWR053" H 8825 975 50  0001 C CNN
F 1 "+5V" H 8825 1265 50  0000 C CNN
F 2 "" H 8825 1125 50  0000 C CNN
F 3 "" H 8825 1125 50  0000 C CNN
	1    8825 1125
	1    0    0    -1  
$EndComp
Wire Wire Line
	8825 1125 8825 1275
Wire Wire Line
	8825 1875 8825 2025
$Comp
L shield-rescue:MCP6002-shield-rescue U2
U 1 1 585943FC
P 8925 1575
F 0 "U2" H 8925 1725 50  0000 L CNN
F 1 "MCP6242" H 8925 1425 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 8825 1625 50  0001 C CNN
F 3 "" H 8925 1725 50  0000 C CNN
	1    8925 1575
	1    0    0    -1  
$EndComp
Text Notes 7975 875  0    60   ~ 0
Not used amp. 
Text Notes 1575 5375 0    60   ~ 0
For 6V batteries : R17 = 100kOhms, R20 = 470kOhms
Text Notes 1575 5550 0    60   ~ 0
30V : R17 = 300kOhms, R20 = 56kOhms\n15V : R17 = 150kOhms, R20 = 56kOhms
Wire Wire Line
	10175 4050 10525 4050
Wire Wire Line
	8425 3950 9125 3950
Wire Wire Line
	8425 3950 8425 4200
Wire Wire Line
	4000 4075 4300 4075
Wire Wire Line
	1825 3975 1825 4075
Wire Wire Line
	1825 3975 2150 3975
Wire Wire Line
	2150 3975 2150 4125
Wire Wire Line
	2150 3975 2950 3975
Wire Wire Line
	7650 5125 7800 5125
Wire Wire Line
	7475 4500 7800 4500
Wire Wire Line
	7475 3950 7475 3975
Wire Wire Line
	1825 4800 1825 4975
Wire Wire Line
	7125 4500 7475 4500
Wire Wire Line
	6625 4500 6425 4500
Wire Wire Line
	6425 4200 6425 4500
Wire Wire Line
	7650 5125 7475 5125
Wire Wire Line
	7475 5125 7475 5100
Connection ~ 7650 5125
Wire Wire Line
	7475 3950 8425 3950
$Comp
L Device:R R21
U 1 1 5BA6960B
P 8225 1675
F 0 "R21" V 8400 1675 50  0000 C CNN
F 1 "R" V 8325 1675 50  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8155 1675 50  0001 C CNN
F 3 "~" H 8225 1675 50  0001 C CNN
	1    8225 1675
	0    1    1    0   
$EndComp
$Comp
L Device:R R20
U 1 1 5BA6971B
P 8225 1475
F 0 "R20" V 8018 1475 50  0000 C CNN
F 1 "R" V 8109 1475 50  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8155 1475 50  0001 C CNN
F 3 "~" H 8225 1475 50  0001 C CNN
	1    8225 1475
	0    1    1    0   
$EndComp
$Comp
L Device:R R22
U 1 1 5BA697B3
P 8800 2500
F 0 "R22" V 9007 2500 50  0000 C CNN
F 1 "R" V 8916 2500 50  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8730 2500 50  0001 C CNN
F 3 "~" H 8800 2500 50  0001 C CNN
	1    8800 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8625 1475 8375 1475
Wire Wire Line
	8375 1675 8500 1675
$Comp
L power:GND #PWR050
U 1 1 5BA6BB54
P 7925 1775
F 0 "#PWR050" H 7925 1525 50  0001 C CNN
F 1 "GND" H 7930 1602 50  0000 C CNN
F 2 "" H 7925 1775 50  0001 C CNN
F 3 "" H 7925 1775 50  0001 C CNN
	1    7925 1775
	1    0    0    -1  
$EndComp
Wire Wire Line
	7925 1475 8075 1475
Wire Wire Line
	8075 1675 7925 1675
Wire Wire Line
	7925 1475 7925 1675
Connection ~ 7925 1675
Wire Wire Line
	7925 1675 7925 1775
Wire Wire Line
	9225 1575 9450 1575
Wire Wire Line
	9450 1575 9450 2500
Wire Wire Line
	9450 2500 8950 2500
Wire Wire Line
	8650 2500 8500 2500
Wire Wire Line
	8500 2500 8500 1675
Connection ~ 8500 1675
Wire Wire Line
	8500 1675 8625 1675
$Comp
L shield-rescue:C-shield-rescue C8
U 1 1 5BB77F85
P 4500 6450
F 0 "C8" H 4250 6550 50  0000 L CNN
F 1 "100nF" H 4250 6350 50  0000 L CNN
F 2 "windlog:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 4538 6300 50  0001 C CNN
F 3 "" H 4500 6450 50  0000 C CNN
	1    4500 6450
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR046
U 1 1 5BB78107
P 4500 6750
F 0 "#PWR046" H 4500 6500 50  0001 C CNN
F 1 "GND" H 4500 6600 50  0000 C CNN
F 2 "" H 4500 6750 50  0000 C CNN
F 3 "" H 4500 6750 50  0000 C CNN
	1    4500 6750
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:+5V-power1 #PWR045
U 1 1 5BB7819E
P 4500 6100
F 0 "#PWR045" H 4500 5950 50  0001 C CNN
F 1 "+5V" H 4500 6240 50  0000 C CNN
F 2 "" H 4500 6100 50  0000 C CNN
F 3 "" H 4500 6100 50  0000 C CNN
	1    4500 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 6750 4500 6600
Wire Wire Line
	4500 6300 4500 6100
Text Notes 8225 2600 0    60   ~ 0
10k pour eviter une boucle ouverte\n
$EndSCHEMATC
